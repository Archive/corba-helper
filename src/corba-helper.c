/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

#include <gtk/gtk.h>
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>

#include "corba-helper.h"

GtkWidget* main_window;
GtkWidget* the_notebook;

typedef struct 
{
	char *name;
	GtkWidget* (*fun)(void);
} page;


page pages[] = {{"Registration browser", object_browser_new},
		{"Name service browser", nameservice_browser_new},
		{"IDL browser", idl_browser_new}};

static void
create_pages (GtkNotebook *notebook)
{
	GtkWidget *child = NULL;
	GtkWidget *label;
	GtkWidget *label_box;
	GtkWidget *menu_box;
	gint i;
	char buffer[32];

	for (i = 0; i < sizeof(pages) / sizeof(page); i++)
	{
		GtkWidget* page_contents = NULL;
	  
		/* make a frame */
		sprintf (buffer, "%s", pages[i].name);
		child = gtk_frame_new (buffer);
		gtk_container_set_border_width (GTK_CONTAINER (child), 10);

		page_contents = pages[i].fun();

		if (page_contents)
			gtk_container_add (GTK_CONTAINER (child), page_contents);
	  
		gtk_widget_show_all (child);

		/* make a label with a named label in it */
		label_box = gtk_hbox_new (FALSE, 0);
		label = gtk_label_new (buffer);
		gtk_box_pack_start (GTK_BOX (label_box), label, FALSE, TRUE, 0);
		gtk_widget_show_all (label_box);

		/* make an hbox with a named label in it */
		menu_box = gtk_hbox_new (FALSE, 0);
		label = gtk_label_new (buffer);
		gtk_box_pack_start (GTK_BOX (menu_box), label, FALSE, TRUE, 0);
		gtk_widget_show_all (menu_box);

		/* put the frame, the label box, and the menu_box
		 * into a new page */
		gtk_notebook_append_page_menu (notebook, child,
					       label_box, menu_box);
	}
} /* create_pages */



void
setup_window ()
{
	/* create main window */
	main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	gtk_signal_connect (GTK_OBJECT (main_window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_main_quit),
			    &main_window);

	gtk_window_set_title (GTK_WINDOW (main_window), "CORBA Helper");

	gtk_container_set_border_width (GTK_CONTAINER (main_window), 0);

	the_notebook = gtk_notebook_new ();

	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (the_notebook), GTK_POS_TOP);
	gtk_container_set_border_width (GTK_CONTAINER (the_notebook), 10);

	gtk_container_add (GTK_CONTAINER (main_window), the_notebook);
		
	gtk_widget_realize (the_notebook);

	create_pages (GTK_NOTEBOOK (the_notebook));
} /* setup_window */


int		
main (int argc, char *argv[])
{
	CORBA_Environment ev;
	CORBA_ORB orb;
	
	CORBA_exception_init(&ev);

	orb = gnome_CORBA_init ("thing", "0.0", &argc, argv,
				0, &ev);

	setup_window();
	
	gtk_widget_show_all (main_window);

	gtk_widget_realize (main_window);
	
	gtk_main ();

        return 0;	
}

