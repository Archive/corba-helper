/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@loper.org>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

struct pix_mask
{
  GdkPixmap *pix;
  GdkBitmap *mask;
};

extern struct pix_mask context_picture;
extern struct pix_mask object_picture;

#define USE_ANTIALIASING

#ifdef USE_ANTIALIASING
 
static char * object_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c #303030",
"X      c #c0c0c0",
"c      c #909090",
"                ",
"                ",
"      XXX       ",
"    c.....c     ",
"   .........    ",
"  ...........   ",
" c...........c  ",
" .............  ",
" .............  ",
" .............  ",
" c...........c  ",
"  ...........   ",
"   .........    ",
"    c.....c     ",
"      XXX       ",
"                "};

static char * context_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c #303030",
"X      c #c0c0c0",
"c      c #909090",
"                ",
"                ",
"       XXX      ",
"     c.....c    ",
"    ...c c...   ",
"   ...     ...  ",
"  c..c     c..c ",
"  ...       ... ",
"  ...       ... ",
"  ...       ... ",
"  c..c     c..c ",
"   ...     ...  ",
"    ...c c...   ",
"     c.....c    ",
"       XXX      ",
"                "};

#else // !USE_ANTIALIASING

static char * object_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c #808080",
"X      c #808080",
"o      c white",
"                ",
"                ",
"                ",
"      .....     ",
"    .........   ",
"   ...........  ",
"   ...........  ",
"  ............. ",
"  ............. ",
"  ............. ",
"   ...........  ",
"   ...........  ",
"    .........   ",
"      .....     ",
"                ",
"                "};

static char * context_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c #808080",
"X      c #808080",
"o      c white",
"                ",
"                ",
"                ",
"      .....     ",
"    ...   ...   ",
"   ...     ...  ",
"   ..       ..  ",
"  ...       ... ",
"  ...       ... ",
"  ...       ... ",
"   ..       ..  ",
"   ...     ...  ",
"    ...   ...   ",
"      .....     ",
"                ",
"                "};
#endif 
static char * book_open_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c black",
"X      c #808080",
"o      c white",
"                ",
"  ..            ",
" .Xo.    ...    ",
" .Xoo. ..oo.    ",
" .Xooo.Xooo...  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
"  .Xoo.Xoo..X.  ",
"   .Xo.o..ooX.  ",
"    .X..XXXXX.  ",
"    ..X.......  ",
"     ..         ",
"                "};

static char * book_closed_xpm[] = {
"16 16 6 1",
"       c None s None",
".      c black",
"X      c red",
"o      c yellow",
"O      c #808080",
"#      c white",
"                ",
"       ..       ",
"     ..XX.      ",
"   ..XXXXX.     ",
" ..XXXXXXXX.    ",
".ooXXXXXXXXX.   ",
"..ooXXXXXXXXX.  ",
".X.ooXXXXXXXXX. ",
".XX.ooXXXXXX..  ",
" .XX.ooXXX..#O  ",
"  .XX.oo..##OO. ",
"   .XX..##OO..  ",
"    .X.#OO..    ",
"     ..O..      ",
"      ..        ",
"                "};

static char * mini_page_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c black",
"X      c white",
"o      c #808080",
"                ",
"   .......      ",
"   .XXXXX..     ",
"   .XoooX.X.    ",
"   .XXXXX....   ",
"   .XooooXoo.o  ",
"   .XXXXXXXX.o  ",
"   .XooooooX.o  ",
"   .XXXXXXXX.o  ",
"   .XooooooX.o  ",
"   .XXXXXXXX.o  ",
"   .XooooooX.o  ",
"   .XXXXXXXX.o  ",
"   ..........o  ",
"    oooooooooo  ",
"                "};

static char * gtk_mini_xpm[] = {
"15 20 17 1",
"       c None",
".      c #14121F",
"+      c #278828",
"@      c #9B3334",
"#      c #284C72",
"$      c #24692A",
"%      c #69282E",
"&      c #37C539",
"*      c #1D2F4D",
"=      c #6D7076",
"-      c #7D8482",
";      c #E24A49",
">      c #515357",
",      c #9B9C9B",
"'      c #2FA232",
")      c #3CE23D",
"!      c #3B6CCB",
"               ",
"      ***>     ",
"    >.*!!!*    ",
"   ***....#*=  ",
"  *!*.!!!**!!# ",
" .!!#*!#*!!!!# ",
" @%#!.##.*!!$& ",
" @;%*!*.#!#')) ",
" @;;@%!!*$&)'' ",
" @%.%@%$'&)$+' ",
" @;...@$'*'*)+ ",
" @;%..@$+*.')$ ",
" @;%%;;$+..$)# ",
" @;%%;@$$$'.$# ",
" %;@@;;$$+))&* ",
"  %;;;@+$&)&*  ",
"   %;;@'))+>   ",
"    %;@'&#     ",
"     >%$$      ",
"      >=       "};





