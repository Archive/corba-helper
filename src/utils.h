/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

#ifndef CORBA_HELPER_UTILS_H
#define CORBA_HELPER_UTILS_H

/* Brings up a simple, modal dialog box with an `OK' button. */
void message_box (gchar *format, ...);

/* Returns TRUE if @str contains a non-whitespace character */
gboolean contains_non_whitespace (gchar *str);

/* Constructs a new string (based on @str) which has no
 * leading or trailing whitespace, and returns that string.*/
gchar* remove_leading_and_trailing_whitespace (gchar *str);

/* Returns true if any character in a string is a whitespace. */
gboolean contains_whitespace (gchar *str);

/* Turns any non-alphanumeric character in a string
 * into an underscore. */
void convert_to_alphanumeric (gchar *str);

#endif // CORBA_HELPER_UTILS_H
