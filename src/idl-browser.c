/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *----------------------------------------------------------------------*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <libIDL/IDL.h>

/* for mmap'ing */
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "idl-bitmaps.h"

#include "corba-helper.h"

extern GtkWidget* main_window;

struct pix_mask function_picture = {NULL,NULL};
struct pix_mask attrib_picture = {NULL,NULL};

/* our main window, which contains... */
GtkWidget *main_idl_wnd = NULL;

/* ...a text box in a scrolled window... */
GtkWidget *text_box, *scrolled_textwin;

/* ...and a tree control, also in a scrolled window */
GtkCTree *idl_root_tree_ctrl = NULL;
GtkWidget *idl_scrolled_treewin = NULL;


#ifdef DO_WINDOWS_SPLITTER_WINDOW
static void
gtk_my_size_allocate (GtkWidget     *widget,
		      GtkAllocation *allocation)
{
	GtkPaned *paned;
	GtkRequisition child1_requisition;
	GtkRequisition child2_requisition;
	GtkAllocation child1_allocation;
	GtkAllocation child2_allocation;
	GdkRectangle old_groove_rectangle;
	guint16 border_width;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_HPANED (widget));
	g_return_if_fail (allocation != NULL);

	widget->allocation = *allocation;

	paned = GTK_PANED (widget);
	border_width = GTK_CONTAINER (paned)->border_width;

	if (paned->child1)
		gtk_widget_get_child_requisition (paned->child1, &child1_requisition);
	else
		child1_requisition.width = 0;

	if (paned->child2)
		gtk_widget_get_child_requisition (paned->child2, &child2_requisition);
	else
		child2_requisition.width = 0;
    
	gtk_paned_compute_position (paned,
				    widget->allocation.width
				    - paned->gutter_size
				    - 2 * border_width,
				    child1_requisition.width,
				    child2_requisition.width);
  
	/* Move the handle before the children so we don't
	   get extra expose events */

	paned->handle_xpos = paned->child1_size + border_width +
		paned->gutter_size / 2 - paned->handle_size / 2;

	paned->handle_ypos = allocation->height - border_width -
		2*paned->handle_size;

	if (GTK_WIDGET_REALIZED (widget))
	{
		GtkWidget *w = GTK_WIDGET(paned);
	    
		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);

		gdk_window_move (paned->handle, paned->handle_xpos, paned->handle_ypos);
		gdk_window_move_resize (paned->handle,
					w->allocation.x,
					w->allocation.y,
					w->allocation.width,
					w->allocation.height);
	}

	child1_allocation.height = child2_allocation.height = MAX (1, (gint)allocation->height - border_width * 2);
	child1_allocation.width = paned->child1_size;
	child1_allocation.x = border_width;
	child1_allocation.y = child2_allocation.y = border_width;
  
	old_groove_rectangle = paned->groove_rectangle;

	paned->groove_rectangle.x = child1_allocation.x 
		+ child1_allocation.width + paned->gutter_size / 2 - 1;
	paned->groove_rectangle.y = 0;
	paned->groove_rectangle.width = 2;
	paned->groove_rectangle.height = allocation->height;
	paned->groove_rectangle.height = 0;	
      
	if (GTK_WIDGET_DRAWABLE (widget) &&
	    ((paned->groove_rectangle.x != old_groove_rectangle.x) ||
	     (paned->groove_rectangle.y != old_groove_rectangle.y) ||
	     (paned->groove_rectangle.width != old_groove_rectangle.width) ||
	     (paned->groove_rectangle.height != old_groove_rectangle.height)))
	{
		gtk_widget_queue_clear_area (widget,
					     old_groove_rectangle.x,
					     old_groove_rectangle.y,
					     old_groove_rectangle.width,
					     old_groove_rectangle.height);
		gtk_widget_queue_draw_area (widget,
					    paned->groove_rectangle.x,
					    paned->groove_rectangle.y,
					    paned->groove_rectangle.width,
					    paned->groove_rectangle.height);
	}
  
	child2_allocation.x = paned->groove_rectangle.x + paned->gutter_size / 2 + 1;
	child2_allocation.width = MAX (1, (gint)allocation->width
				       - child2_allocation.x - border_width);
  
	/* Now allocate the childen, making sure, when resizing not to
	 * overlap the windows */
	if (GTK_WIDGET_MAPPED(widget) &&
	    paned->child1 && GTK_WIDGET_VISIBLE (paned->child1) &&
	    paned->child1->allocation.width < child1_allocation.width)
	{
		if (paned->child2 && GTK_WIDGET_VISIBLE (paned->child2))
			gtk_widget_size_allocate (paned->child2,
						  &child2_allocation);

		gtk_widget_size_allocate (paned->child1, &child1_allocation);      
	}
	else
	{
		if (paned->child1 && GTK_WIDGET_VISIBLE (paned->child1))
			gtk_widget_size_allocate (paned->child1,
						  &child1_allocation);

		if (paned->child2 && GTK_WIDGET_VISIBLE (paned->child2))
			gtk_widget_size_allocate (paned->child2,
						  &child2_allocation);
	}
} /* gtk_my_size_allocate */
#endif // DO_WINDOWS_SPLITTER_WINDOW


/* returns an mmap'd file readonly, and puts its size in `filesize';
 * of course, you still have to munmap when you're done */
static gchar*
map_file(const gchar *filename, size_t *filesize)
{
	struct stat statbuf;
	gchar *file_contents = NULL;
	int file_fd = open(filename, O_RDONLY);

	g_assert(filename && filesize);

	memset(&statbuf, 0, sizeof(statbuf));
	
	if (file_fd == -1) {
		perror(filename);
		return NULL;
	}

	if (fstat(file_fd, &statbuf) < 0) {
		perror("fstat");
		return NULL;
	}
	
	file_contents = mmap(
		0, statbuf.st_size,
		PROT_READ, MAP_FILE | MAP_SHARED,
		file_fd, 0);

	if (file_contents == (caddr_t) -1) {
		perror("mmap");
		return NULL;
	}

	*filesize = statbuf.st_size;
	
	return file_contents;
} /* map_file */

 

/* clears the contents of a gtk_text_box, and inserts the contents the file
 * represented by `filename' */
static void
gtk_text_box_insert_file(gchar *filename, GtkWidget* the_text_box)
{
        GdkFont *font; 
        gchar *file_contents;
	size_t file_size;

	gtk_editable_delete_text (
		GTK_EDITABLE (the_text_box), 0,
		gtk_text_get_length (GTK_TEXT (the_text_box)) - 1);


	if (NULL == (file_contents = map_file(filename, &file_size))) {
		g_print ("map_file failed!\n");
		return;
	}
	
        font = gdk_font_load ("-*-courier-medium-r-normal--*-110-*-*-*-*-*-*");

	gtk_text_insert (GTK_TEXT (the_text_box), font, NULL,
			 NULL, file_contents, file_size);

	if (munmap(file_contents, file_size) == -1) {
		perror("munmap");
		exit(0);
	}

	gdk_font_unref (font); /* the gtk_text has addref'd the font */	
		
        gtk_text_thaw (GTK_TEXT (the_text_box));

} /* gtk_text_box_insert_file */


/* when a branch is clicked on in the idl_root_tree_ctrl, this callback
 * makes sure that the correct file is displayed in the righthand text_box */
static void
my_tree_select_row_cb   (GtkCTree     *ctree,
			 GtkCTreeNode *row,
			 gint          column)
{
        GString *filename;

        g_assert(ctree && row);
        filename = gtk_ctree_node_get_row_data(ctree, row);

        gtk_text_box_insert_file (filename->str, text_box);

	/* if we're a toplevel window, set our title */
	if (GTK_IS_WINDOW (main_idl_wnd))
		gtk_window_set_title (
			GTK_WINDOW (main_idl_wnd),
			g_basename(filename->str));

/* we don't free 'filename', because it belongs to the treectrl row */

} /* my_tree_select_row_cb */



static void
file_selection_ok (GtkWidget        *w,
		   GtkFileSelection *fs)
{
	char *argv[] = 
	{"", gtk_file_selection_get_filename(fs)};
	
	idl_browser_new_with_filenames(2, argv);
	
	gtk_widget_destroy (GTK_WIDGET (fs));
} /* file_selection_ok */

static void
create_file_selection(GtkWidget *unused1,
		      gpointer *unused2)
{
  static GtkWidget *window = NULL;

  if (!window)
    {
      window = gtk_file_selection_new ("file selection dialog");

      gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));

      gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);

      gtk_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			  &window);

      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
			  "clicked", GTK_SIGNAL_FUNC(file_selection_ok),
			  window);

      gtk_signal_connect_object (
	      GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
	      "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
	      GTK_OBJECT (window));
      
    }
  
  
  if (!GTK_WIDGET_VISIBLE (window))
    gtk_widget_show (window);
  else
    gtk_widget_destroy (window);

} /* create_file_selection */


/* creates a main window and puts root_tree_control and text_box in it,
 * initializes some pixmaps we'll use for icons in the tree ctrl,
 * and returns the main window */
static GtkWidget*
setup_idl_window()
{
        GtkWidget *vbox, *hpane;
        GdkColor transparent;
	GtkWidget *load_btn;
	GtkWidget *hbox;

        vbox = gtk_vbox_new (FALSE, 0);

        hpane = gtk_hpaned_new();

#ifdef DO_WINDOWS_SPLITTER_WINDOW
	gtk_signal_connect(GTK_OBJECT(hpane), "size_allocate",
			   gtk_my_size_allocate, &hpane);
#endif // DO_WINDOWS_SPLITTER_WINDOW
	gtk_box_pack_start (GTK_BOX (vbox), hpane, TRUE, TRUE, 0);

	load_btn = gtk_button_new_with_label("Load IDL file");
	gtk_signal_connect (GTK_OBJECT (load_btn), "clicked",
			    GTK_SIGNAL_FUNC(create_file_selection),
			    NULL);

	hbox = gtk_hbox_new (FALSE, 0);
	
	gtk_box_pack_start (GTK_BOX (hbox), load_btn, TRUE, FALSE, 0);		
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

        /* make a scrolled window for the treectrl, and put it in the hpane */
        idl_scrolled_treewin = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_set_border_width (
		GTK_CONTAINER (idl_scrolled_treewin), 5);
        gtk_scrolled_window_set_policy (
		GTK_SCROLLED_WINDOW (idl_scrolled_treewin),
		GTK_POLICY_AUTOMATIC,
		GTK_POLICY_ALWAYS);

        gtk_paned_add1 (GTK_PANED (hpane), idl_scrolled_treewin);

        /* make a ctree control & put it in the scrolled window */
        idl_root_tree_ctrl = GTK_CTREE (gtk_ctree_new(2, 0));

        gtk_container_add (GTK_CONTAINER (idl_scrolled_treewin),
                           GTK_WIDGET (idl_root_tree_ctrl));

        /* make a scrolled window for the edit box, and put it in the hpane */
        scrolled_textwin = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_set_border_width (GTK_CONTAINER (scrolled_textwin), 5);
        gtk_scrolled_window_set_policy (
		GTK_SCROLLED_WINDOW (scrolled_textwin),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

#if 0 // show editor window
        gtk_paned_add2 (GTK_PANED (hpane), scrolled_textwin);
#endif
        /* make a text window, and put it in its scrolled_textwin */
        text_box = gtk_text_new(NULL, NULL);
        gtk_container_add (GTK_CONTAINER (scrolled_textwin), text_box);
        gtk_widget_set_usize (GTK_WIDGET (text_box),
			      TREECTRL_WIDTH, TREECTRL_HEIGHT);
        gtk_widget_grab_focus (text_box);
        gtk_widget_show (text_box);
        
        /* configure the tree ctrl */
        gtk_clist_set_column_auto_resize (GTK_CLIST (idl_root_tree_ctrl), 0, TRUE);
        gtk_clist_set_column_width (GTK_CLIST (idl_root_tree_ctrl), 1, 200);
        gtk_clist_set_selection_mode (
		GTK_CLIST (idl_root_tree_ctrl),
		GTK_SELECTION_SINGLE);
	
        gtk_ctree_set_line_style (idl_root_tree_ctrl, GTK_CTREE_LINES_DOTTED);

	g_assert(main_window->window);
	
        /* create the pixmaps that will go to the left of treectrl items */
        if (!function_picture.pix)
		function_picture.pix = gdk_pixmap_create_from_xpm_d (
			main_window->window, &function_picture.mask,
			&transparent, function_xpm);

	if (!attrib_picture.pix)
		attrib_picture.pix = gdk_pixmap_create_from_xpm_d (
			main_window->window, &attrib_picture.mask,
			&transparent, attrib_xpm);

        gtk_signal_connect_after (
		GTK_OBJECT (idl_root_tree_ctrl),
		"tree_select_row",
		GTK_SIGNAL_FUNC (my_tree_select_row_cb), NULL);

        /* set the width and height of the tree */
        gtk_widget_set_usize (GTK_WIDGET (idl_root_tree_ctrl), 300, 300);

        return vbox;
} /* setup_idl_window */

/* when a ctree branch is destroyed, it should destroy it's associated
 * data, which is a string; this is the callback that does that */
static void
my_g_string_free_cb(gpointer data)
{
        g_string_free(data, TRUE);
}

/* creates a labeled subtree ( = GtkCTreeNode) in a GtkCTree ctrl, 
 * associates `filename' with that node, and returns it */
static GtkCTreeNode*
make_subtree(GtkCTree *root_treectrl, // required
	     GtkCTreeNode *mama_treenode, // can be NULL
	     gchar *label,
	     gboolean starts_open, 
	     const gchar *filename, 
	     GdkPixmap *pix_open, GdkBitmap *msk_open,
	     GdkPixmap *pix_closed, GdkBitmap *msk_closed) 
{
        GtkCTreeNode *new_subtree;
        GString *filename_copy;
        gchar *text [2] = { label, "" };

        g_assert(filename);

        filename_copy = g_string_new(filename);

        new_subtree = gtk_ctree_insert_node (
                root_treectrl, mama_treenode,
                NULL, text, 5,
                pix_closed, msk_closed,
                pix_open, msk_open,
                FALSE,
                starts_open);

        gtk_ctree_node_set_row_data_full(
                idl_root_tree_ctrl, new_subtree,
                filename_copy, /* user data (in this case a filename) */
                my_g_string_free_cb); /* destructor function for data */

/* we don't free filename_copy, because it's
 * now owned by the ctreectrl row */
	
        return new_subtree;

} /* make_subtree */

/* this function is called for every node under an interface;
 * but it just collects those directly below
 * an interface (ie depth == 1), and puts 'em in
 * our tree control */
static gboolean
gather_elements_in_interface (IDL_tree_func_data *tfd, 
			      GtkCTreeNode *parent_tree_node)
{
        IDL_tree_func_data *parent;
        IDL_tree pIDL_node;
        int depth = 0;
        GdkPixmap *icon_for_branch = NULL;
        GdkBitmap *icon_for_branch_mask = NULL;
	GString *this_element_name;
	GtkCTreeNode *tree_node;
	
        pIDL_node = tfd->tree;

        /* figure out how deep we are in the idl tree
         * we've been handed */
        parent = tfd->up;
        while (parent!=NULL)
        {
                IDL_tree parent_tree;

                depth++;
                parent_tree = parent->tree;
                parent = parent->up;
        }

	if (depth!=1)
		return TRUE;
	
        /* if this branch is a function or attribute, set its icon */
        switch (pIDL_node->_type) {
	case IDLN_OP_DCL: // function
		icon_for_branch = function_picture.pix;
		icon_for_branch_mask = function_picture.mask;
		break;
	case IDLN_ATTR_DCL: // attribute
		icon_for_branch = attrib_picture.pix;
		icon_for_branch_mask = attrib_picture.mask;
		break;
	default:
		break;
		
        }

        
	this_element_name = IDL_tree_to_IDL_string (pIDL_node, NULL,
						    IDLF_OUTPUT_NO_NEWLINES);  

	tree_node = make_subtree(
		idl_root_tree_ctrl,
		parent_tree_node, this_element_name->str, FALSE,
		pIDL_node->_file,
		icon_for_branch, icon_for_branch_mask,
		icon_for_branch, icon_for_branch_mask);

	g_string_free(this_element_name, TRUE);

// if you want to see every IDL_tree subelement recursively, change this to 1
#if 0
	IDL_tree_walk_in_order (
		pIDL_node,
		(IDL_tree_func) gather_elements_in_interface, 
		tree_node);
#endif
	return FALSE;
} /* gather_elements_in_interface */

/* this function is given all the elements ( = nodes) from an .idl file,
 * but it's only interested in interfaces; so it gathers those in our
 * tree control and then delegates the collection of its subelements
 * (which would be the attributes, functions etc. of an interface)
 * to gather_elements_in_interface() */
static gboolean
gather_interfaces (IDL_tree_func_data *tfd, GtkCTreeNode *parent_tree_node)
{
        IDL_tree p;
        GString *interface_name, *what_it_inherits_from;
        GtkCTreeNode* tree_node;

        p = tfd->tree;

        /* we're only interested in interface nodes */
        if (IDL_NODE_TYPE (p) != IDLN_INTERFACE)
		return TRUE;
	
	interface_name = IDL_tree_to_IDL_string (
		p->u.idl_interface.ident,
		NULL, IDLF_OUTPUT_NO_NEWLINES);

	/* if this interface inherits from other(s)... */
	if (p->u.idl_interface.inheritance_spec) {

		/* append them to our interface string */
		what_it_inherits_from = IDL_tree_to_IDL_string(
			p->u.idl_interface.inheritance_spec,
			NULL, IDLF_OUTPUT_NO_NEWLINES);  

		interface_name = g_string_append(interface_name, " : ");
		interface_name = g_string_append(
			interface_name,
			what_it_inherits_from->str);
      
		/* and make a branch from it */
		tree_node = make_subtree(
			idl_root_tree_ctrl,
			parent_tree_node,
			interface_name->str, FALSE, p->_file,
			NULL, NULL, NULL, NULL);
		
		g_string_free(what_it_inherits_from, TRUE);
	}
	else /* this interface inherits from no-one, so                 */
	{    /* make a branch that only has the interface name as label */
		tree_node = make_subtree(
			idl_root_tree_ctrl, parent_tree_node,
			interface_name->str, FALSE, p->_file,
			NULL, NULL, NULL, NULL);
	}
	g_string_free(interface_name, TRUE);

	IDL_tree_walk_in_order (
		p->u.idl_interface.body,
		(IDL_tree_func) gather_elements_in_interface, 
		tree_node);

	return FALSE;
} /* gather_interfaces */


GtkWidget*
idl_browser_new_with_filenames(int argc, char *argv[])
{
	gint i;
	gboolean bFirstFound = TRUE;

	if (!main_idl_wnd)
		main_idl_wnd = setup_idl_window();

        /* all commandline options should have been .idl filenames; 
         * iterate through them and add a treectrl branch for each */
        for (i = 1; i<argc; i++)
        {
                /* vars for idl processing with libIDL */
                int rv = 0; // return value
                IDL_ns ns = NULL; // namespace
                IDL_tree my_idl_tree = NULL; // a node within an idl-tree

		/* process idl file */
                rv = IDL_parse_filename (
                        /* idl filename */argv[i], "-I/usr/share/idl", NULL,
                        /* out */&my_idl_tree, &ns,
                        /* parseflags */ IDLF_PREFIX_FILENAME|IDLF_TYPECODES,
                        IDL_WARNING1);

                if (rv == IDL_ERROR || rv < 0) {
                        if (rv == IDL_ERROR)
                                g_print ("IDL_ERROR for file %s\n", argv[i]);
                        else {
                                GString *err_string = g_string_new("");
                                g_string_sprintf(
					err_string,
					"couldn't find filename %s",
					argv[i]);
				
                                perror (err_string->str/* idl filename */);
                                g_string_free(err_string, TRUE);
                        }
                }
                else { /* we parsed the .idl file correctly */
                        GtkCTreeNode* tree_node;

                        if (bFirstFound) {
                                GString *idl_contents;

                                /* IDL_tree_to_IDL (
				   my_idl_tree, ns, stdout, 0); */
				
                                idl_contents = IDL_tree_to_IDL_string (
					my_idl_tree, NULL, 0);  

                                bFirstFound = FALSE;

                                gtk_text_box_insert_file(argv[i], text_box);
                        }

			tree_node = make_subtree(idl_root_tree_ctrl, NULL, 
                                                 g_basename(argv[i]),
						 argc==2, argv[i],
						 NULL, NULL, NULL, NULL);
	

                        /* walk our tree of modules, interfaces, etc,
                         * and collect the 'interface' nodes in 
                         * the function "gather_interfaces"; this places
                         * interfaces and their internals in the tree ctrl */
                        IDL_tree_walk_in_order (
                                my_idl_tree, 
                                (IDL_tree_func) gather_interfaces, 
                                tree_node);
	
                        IDL_ns_free(ns);
                        IDL_tree_free(my_idl_tree);
                }
        }
	return main_idl_wnd;
	
} /* idl_browser_new_with_filenames */

GtkWidget*
idl_browser_new()
{
	return idl_browser_new_with_filenames(0, NULL);
} 



