/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

#include <gnome.h>
#include <libgnorba/gnorba.h>
#include "corba-helper.h"
#include "uuid.h"

#include "utils.h" /* string functions */

/* for writing to .gnorba files */
#include <sys/stat.h>
#include <errno.h>

typedef char uuid_string[37];

typedef struct 
{
	GtkWidget **main_window;

	/* interface information */
	GHashTable *iface_idl_hash;
	GHashTable *iface_dce_hash;
	GHashTable *iface_local_hash;
	GtkCombo   *iface_combo; /* interfaces here can be added to... */
	GtkCList   *iface_clist; /* this clist */
	GtkButton  *iface_add_uuid_button;

	/* server information */
	GtkEntry *server_entry;

	/* creation method information */
	enum { CREATION_METHOD_FACTORY,
	       CREATION_METHOD_SHLIB,
	       CREATION_METHOD_EXE }
	   creation_method;

	GtkCombo  *creation_combo;
	GtkButton *creation_browse_button;

	/* description info */
	GtkEntry *description_entry;

} object_registration_info;

void get_new_uuid(uuid_string string_out)
{
	uuid_t our_uuid;
	
	uuid_generate(our_uuid);
	uuid_unparse(our_uuid, string_out);
} /* get_new_uuid */

static void
delete_keys (gpointer key, gpointer value, gpointer user)
{
	g_free(key);
} /* delete_keys */


void
delete_reg_info(GtkWidget* w, gpointer p)
{
	object_registration_info *reg_info = p;
	g_assert (p);

	g_hash_table_foreach (reg_info->iface_idl_hash,
			      delete_keys,
			      NULL);
	g_hash_table_foreach (reg_info->iface_dce_hash,
			      delete_keys,
			      NULL);
	g_hash_table_foreach (reg_info->iface_local_hash,
			      delete_keys,
			      NULL);	

	g_hash_table_destroy(reg_info->iface_idl_hash);
	g_hash_table_destroy(reg_info->iface_dce_hash);
	g_hash_table_destroy(reg_info->iface_local_hash);	
	
	(*reg_info->main_window) = NULL;

	g_free(reg_info);
} /* delete_reg_info */

static void
insert_into_combobox (gpointer key, gpointer value, gpointer user)
{
	GtkCombo *combo = user;
	GtkWidget *li;
	gchar *key_string = key;

	li = gtk_list_item_new_with_label (key_string);
	gtk_widget_show (li);
	gtk_container_add (GTK_CONTAINER (combo->list), li);
} /* insert_into_combobox */

	
 
gboolean
register_object (gchar *server_id, gchar *creation_type, gchar **interfaces,
		 gchar *description, gchar *location_info,
		 gboolean user_interaction_allowed)
{
	gchar *filename;
	gchar *filename_fq;
	struct stat file_info;
	gboolean rtn = FALSE;
	int fopen_rtn;

	g_assert (server_id && creation_type && interfaces &&
		  *interfaces && description && location_info);

	/* creation_type will be exe, factory or shlib */
 	if (!contains_non_whitespace (creation_type)) {
		if (user_interaction_allowed)
			message_box ("Please fill in the creation type.");
		return FALSE;
	}
	/* location_info will be a filename or the goad_id of a factory */
 	if (!contains_non_whitespace (location_info)) {
		if (user_interaction_allowed)
			message_box ("Please fill in the creation method.");
		return FALSE;
	}


        /* figure out a good filename and server_id */
	filename = remove_leading_and_trailing_whitespace (server_id);
	
	if (filename) convert_to_alphanumeric (filename);
	
	if (!filename) {
		if (user_interaction_allowed)
			message_box ("The server_id is empty.");
		return FALSE;
	}
	
	if (strlen (filename) < 1) {
		if (user_interaction_allowed)
			message_box ("The server_id is empty.");
		g_free (filename);
		return FALSE;
	}

	filename_fq = g_strdup_printf ("/usr/local/etc/CORBA/servers/%s.gnorba",
				       filename);

	fopen_rtn = stat(filename_fq, &file_info);

	/* if the file doesn't exist yet, we're good to go */
	if (-1 == fopen_rtn && errno == ENOENT) {
		
		FILE *f;
		
		if (f = fopen (filename_fq, "w")) {
			g_print ("writing to file %s\n", filename_fq);

			fprintf (f, "[%s]\ntype=%s\nrepo_id=",
				 server_id, creation_type);

			for (;*interfaces; *interfaces++) {
				fprintf (f, "%s ", *interfaces);
			}

			fprintf (f, "\ndescription=%s\nlocation_info=%s\n",
				 description, location_info);
			rtn = TRUE;
			fclose(f);
			
		}
		else /* fopen failed */
			if (user_interaction_allowed) {
				message_box ("Can't open file %s for writing: %s",
					     filename_fq, strerror(errno));
			}
		
	} else if (fopen_rtn == 0) /* file exists, so let's not overwrite it */
		if (user_interaction_allowed) 
			message_box ("The file %s already exists!",
				     filename_fq);

		else
			message_box (strerror (errno));
	
	g_free (filename);
	g_free (filename_fq);
	return rtn;
} /* register_object */

		

static void
register_object_clicked (GtkWidget *button,
			 gpointer p)
{
	object_registration_info *reg_info = p;
	int num_interfaces = reg_info->iface_clist->rows;
	gboolean quit_this_dlg;
	
	gchar **interfaces = malloc (sizeof (gchar*) * (num_interfaces + 1));
	gchar *server_id = gdk_wcstombs (reg_info->server_entry->text);
	gchar *description = gdk_wcstombs (reg_info->description_entry->text);
	gchar *location_info =
		gdk_wcstombs (GTK_ENTRY (reg_info->creation_combo->entry)->text);
	gchar *creation_type;

	interfaces[num_interfaces] = NULL;
	
	switch (reg_info->creation_method) {
	case CREATION_METHOD_FACTORY:
		creation_type = "factory";
		break;
	case CREATION_METHOD_SHLIB:
		creation_type = "shlib";
		break;
	case CREATION_METHOD_EXE:
		creation_type = "exe";
		break;
	default:
		g_assert_not_reached();
	}

	/* get the row text out of `iface_clist' and into `interfaces' */
	{
		GtkCList *clist = reg_info->iface_clist;
		
		GtkCListRow *clist_row;
		int i;

		for (i = 0; i < clist->rows; i++)
		{
			clist_row = (g_list_nth (clist->row_list, i))->data;
			interfaces[i] = clist_row->cell->u.text;
		}	

	}

	quit_this_dlg = register_object (server_id, creation_type, interfaces,
					 description, location_info, TRUE);
	
	g_free (interfaces);
	g_free (server_id);
	g_free (location_info);
	g_free (description);

	if (quit_this_dlg)
		gtk_widget_destroy (*reg_info->main_window);
	
	
} /* register_object_clicked */


static void
interface_idl_clicked(GtkWidget *refresh_button,
		      gpointer p)
{
	object_registration_info *reg_info = p;
	
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active)
		return;

	gtk_widget_hide (GTK_WIDGET (reg_info->iface_add_uuid_button));
	
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (
		reg_info->iface_combo)->list),
			      0, -1);
	
	g_hash_table_foreach (reg_info->iface_idl_hash,
			      insert_into_combobox,
			      reg_info->iface_combo);

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO(
		reg_info->iface_combo)->entry),
			    "IDL:");	
	
	return;
} /* interface_idl_clicked */

static void
add_interface_to_list(GtkWidget *refresh_button,
		      gpointer p)
{
	object_registration_info* reg_info = p;
	GtkEntry* entry = GTK_ENTRY (GTK_COMBO (reg_info->iface_combo)->entry);
	gchar *text = gtk_entry_get_text (entry);
	gchar *text_to_change[] = { text };
	
	gtk_clist_prepend (reg_info->iface_clist,
			   text_to_change);
} /* add_interface_to_list */


static void
interface_dce_clicked(GtkWidget *refresh_button,
		      gpointer p)
{
	object_registration_info *reg_info = p;
	
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active)
		return;

	gtk_widget_show (GTK_WIDGET (reg_info->iface_add_uuid_button));
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (reg_info->iface_combo)->list),
			      0, -1);
	
	g_hash_table_foreach (reg_info->iface_dce_hash,
			      insert_into_combobox,
			      reg_info->iface_combo);

	gtk_entry_set_text (GTK_ENTRY (reg_info->iface_combo->entry),
			    "DCE:");
	
	
	return;
} /* interface_dce_clicked */

static void
add_uuid_clicked(GtkWidget *refresh_button,
		 GtkEntry *entry)
{
	gchar string[41];

	sprintf(string, "DCE:");
	get_new_uuid(string + 4);
	gtk_entry_set_text (entry, string);
} /* add_uuid_clicked */


static void
interface_local_clicked(GtkWidget *refresh_button,
			gpointer p)
{
	object_registration_info *reg_info = p;
	
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;

	gtk_widget_hide (GTK_WIDGET (reg_info->iface_add_uuid_button));	
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (reg_info->iface_combo)->list),
			      0, -1);
	
	g_hash_table_foreach (reg_info->iface_local_hash,
			      insert_into_combobox,
			      reg_info->iface_combo);

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO(reg_info->iface_combo)->entry),
			    "");
	
	
	return;
} /* interface_local_clicked */


static gint
delete_clist_item (GtkWidget *widget, 
		   GdkEventKey *event,
		   gpointer p)
{
	GtkCList *clist = GTK_CLIST (widget);
	gint row;

	if (event->keyval != GDK_Delete)
		return;
	
	if (clist->selection) {	
		row = (gint)g_list_nth (clist->selection, 0)->data;
		gtk_clist_remove (clist, row);
	}
} /* delete_clist_item */



GtkWidget*
interface_section_new(object_registration_info* reg_info)
{
	GtkWidget *hbox, *vbox;
	GtkWidget *radio_button, *idl_radio_button;
	GtkWidget *button;
	gchar *clist_append_text[1];
	GtkWidget *scrolly;


	/* make an hbox for the radio buttons to live in */
	hbox = gtk_hbox_new (FALSE, 0);

	/* put in 3 radio buttons (idl, dce and local) */
	idl_radio_button = gtk_radio_button_new_with_label (NULL, "IDL");
	gtk_signal_connect (GTK_OBJECT (idl_radio_button), "clicked",
			    GTK_SIGNAL_FUNC(interface_idl_clicked),
			    reg_info);			

	gtk_box_pack_start (GTK_BOX (hbox), idl_radio_button, FALSE, TRUE, 0);

	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (idl_radio_button)),
		"DCE");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(interface_dce_clicked),
			    reg_info);
	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);

	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"Local");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(interface_local_clicked),
			    reg_info);			
	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);

	button = gtk_button_new_with_label ("New");
	reg_info->iface_combo = GTK_COMBO (gtk_combo_new());
	
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC(add_uuid_clicked),
			    GTK_ENTRY (GTK_COMBO(reg_info->iface_combo)->entry));
	
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	reg_info->iface_add_uuid_button = GTK_BUTTON (button);

	/* put that hbox in a vbox, which the rest of our stuff
	   will live in */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 5);


	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (idl_radio_button),
				      TRUE);

	hbox = gtk_hbox_new (FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 5);
	
	gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (reg_info->iface_combo),
			    FALSE, TRUE, 5);
	
	interface_idl_clicked(idl_radio_button, reg_info);	

	button = gtk_button_new_with_label ("Add");
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC(add_interface_to_list),
			    reg_info);			
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 5);
	
	scrolly = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolly),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	
	reg_info->iface_clist = GTK_CLIST (gtk_clist_new(1));
	gtk_signal_connect ( GTK_OBJECT (reg_info->iface_clist), "key_press_event",
			     GTK_SIGNAL_FUNC(delete_clist_item), reg_info);
	
	clist_append_text[0] = "";
	
	gtk_clist_append (GTK_CLIST (reg_info->iface_clist), clist_append_text);	
	gtk_clist_append (GTK_CLIST (reg_info->iface_clist), clist_append_text);
	gtk_container_add (GTK_CONTAINER (scrolly),
			   GTK_WIDGET (reg_info->iface_clist));
	
	gtk_box_pack_start (GTK_BOX (vbox), scrolly, FALSE, TRUE, 5);	
	
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 8);
	return vbox;

} /* interface_section_new */

static void
server_local_clicked(GtkWidget *refresh_button,
		     gpointer p)
{
	object_registration_info* reg_info = p;

	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;	
	gtk_entry_set_text (GTK_ENTRY (reg_info->server_entry),
			    "");
} /* server_local_clicked */

static void
server_dce_clicked(GtkWidget *refresh_button,
		   gpointer p)
{
	object_registration_info* reg_info = p;
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;
	
	gtk_entry_set_text (GTK_ENTRY (reg_info->server_entry),
			    "DCE:");	

} /* server_dce_clicked */

static void
server_obj_clicked(GtkWidget *refresh_button,
		   gpointer p)
{
	object_registration_info* reg_info = p;
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;
	
//	gtk_entry_set_text (GTK_ENTRY (reg_info->server_entry),
//			   "OBJ:");
} /* server_obj_clicked */

		

GtkWidget*
server_id_section_new(object_registration_info* reg_info)
{
	GtkWidget *vbox, *hbox;
	GtkWidget *radio_button;

	vbox = gtk_vbox_new (FALSE, 0);
	hbox = gtk_hbox_new (FALSE, 0);

	reg_info->server_entry = GTK_ENTRY (gtk_entry_new());

	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 5);

	radio_button = gtk_radio_button_new_with_label (
		NULL, "OBJ");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC (server_obj_clicked),
			    reg_info);
	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);
	server_obj_clicked (radio_button, reg_info);
	
	
	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"DCE");

	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(server_dce_clicked),
			    reg_info);

	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);

	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"Local");

	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(server_local_clicked),
			    reg_info);

	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);		
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (reg_info->server_entry),
			    FALSE, TRUE, 0);
	
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 8);

	return vbox;
} /* server_id_section_new */

static void
activation_exe_clicked(GtkWidget *refresh_button,
		       gpointer p)
{
	object_registration_info* reg_info = p;
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;
	reg_info->creation_method = CREATION_METHOD_EXE;
	gtk_widget_show (GTK_WIDGET (reg_info->creation_browse_button));
	
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (reg_info->creation_combo)->list),
			      0, -1);		
} /* activation_exe_clicked */

static void
activation_shlib_clicked(GtkWidget *refresh_button,
			 gpointer p)
{
	object_registration_info* reg_info = p;
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;	
	reg_info->creation_method = CREATION_METHOD_SHLIB;
	gtk_widget_show (GTK_WIDGET (reg_info->creation_browse_button));
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (reg_info->creation_combo)->list),
			      0, -1);		
} /* activation_shlib_clicked */

static void
activation_factory_clicked(GtkWidget *refresh_button,
			   gpointer p)
{
	object_registration_info* reg_info = p;
	if (!(GTK_TOGGLE_BUTTON(refresh_button))->active) return;
	
	reg_info->creation_method = CREATION_METHOD_FACTORY;
	
	gtk_widget_hide (GTK_WIDGET (reg_info->creation_browse_button));

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO(reg_info->creation_combo)->entry),
			    "");
	gtk_list_clear_items (GTK_LIST (GTK_COMBO (reg_info->creation_combo)->list),
			      0, -1);

	
} /* activation_factory_clicked */

void
activation_file_selection_ok (GtkWidget *w,
			      object_registration_info *reg_info)
{
	/* put the filename into the creation_combo of the main reg window */
	GtkFileSelection *fs = GTK_FILE_SELECTION (gtk_widget_get_toplevel (w));
	gtk_entry_set_text (
		GTK_ENTRY (reg_info->creation_combo->entry),
		gtk_file_selection_get_filename (fs));

	/* we're done with the file selection dlg, so... */
	gtk_widget_destroy (GTK_WIDGET (fs));
} /* activation_file_selection_ok */


void
create_activation_type_file_dialog(GtkWidget *unused1,
				   gpointer p)
{
	object_registration_info* reg_info = p;
	GtkFileSelection *fs = gtk_file_selection_new ("file selection dialog");

	gtk_file_selection_hide_fileop_buttons (fs);

	gtk_window_set_position (GTK_WINDOW (fs),
				 GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT (fs->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC(activation_file_selection_ok),
			    reg_info);

	gtk_signal_connect_object (GTK_OBJECT (fs->cancel_button),
				   "clicked",
				   GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT (fs));
      
	if (!GTK_WIDGET_VISIBLE (fs)) {
		gtk_window_set_modal (GTK_WINDOW (fs), TRUE);
		gtk_widget_show (fs);
	}
} /* create_activation_type_file_dialog */


static void
activation_type_browse(GtkWidget *refresh_button,
		       gpointer p)
{
	object_registration_info* reg_info = p;
	create_activation_type_file_dialog(NULL, reg_info);
} /* activation_type_browse */

GtkWidget*
creation_type_section_new(object_registration_info* reg_info)
{
	GtkWidget *vbox, *hbox;
	GtkWidget *radio_button;
	GtkWidget *factory_radiobutton;

	vbox = gtk_vbox_new (FALSE, 0);
	hbox = gtk_hbox_new (FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 5);

	radio_button = gtk_radio_button_new_with_label (
		NULL, "factory");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC (activation_factory_clicked),
			    reg_info);
	factory_radiobutton = radio_button;
	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);
	
	
	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"exe");

	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(activation_exe_clicked),
			    reg_info);

	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);

	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"shlib");

	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC(activation_shlib_clicked),
			    reg_info);

	gtk_box_pack_start (GTK_BOX (hbox), radio_button, FALSE, TRUE, 0);

	reg_info->creation_browse_button = GTK_BUTTON (
		gtk_button_new_with_label ("Browse"));
	gtk_signal_connect (GTK_OBJECT (reg_info->creation_browse_button), "clicked",
			    GTK_SIGNAL_FUNC(activation_type_browse),
			    reg_info);

	gtk_box_pack_start (GTK_BOX (hbox),
			    GTK_WIDGET (reg_info->creation_browse_button),
			    FALSE, TRUE, 0);

	reg_info->creation_combo = GTK_COMBO (gtk_combo_new());

	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (reg_info->creation_combo),
			    FALSE, TRUE, 0);

	gtk_container_set_border_width (GTK_CONTAINER (vbox), 8);

	activation_factory_clicked (factory_radiobutton, reg_info);
	
	return vbox;
} /* creation_type_section_new */

void
fill_interface_hashtables (object_registration_info *reg_info)
{
	int i;
	GoadServerList *servlist;
	GoadServer* servers;

	servlist = goad_server_list_get ();
	servers = servlist->list;
	
	reg_info->iface_idl_hash = g_hash_table_new (g_str_hash, g_str_equal);
	reg_info->iface_dce_hash = g_hash_table_new (g_str_hash, g_str_equal);
	reg_info->iface_local_hash = g_hash_table_new (g_str_hash, g_str_equal);

	
	for (i = 0; servers[i].repo_id; i++)
	{
		int j;
		for (j = 0; servers[i].repo_id[j]; j++)
		{
			int bFound = FALSE;
//		FIXME: only need one hashtable for this...?
			if (strncmp(servers[i].repo_id[j], "IDL:", 4) == 0) {
				g_hash_table_insert(
					reg_info->iface_idl_hash,
					g_strdup(servers[i].repo_id[j]),
					(void*)1);
				bFound = TRUE;
			}
			else if (strncmp(servers[i].repo_id[j], "DCE:", 4) == 0) {
				g_hash_table_insert(
					reg_info->iface_dce_hash,
					g_strdup(servers[i].repo_id[j]),
					(void*)1);
				bFound = TRUE;
			}
				
			else
				g_hash_table_insert (
					reg_info->iface_local_hash,
					g_strdup(servers[i].repo_id[j]),
					(void*)1);
		}
	}

	goad_server_list_free (servlist);
} /* fill_interface_hashtables */


GtkWidget*
object_browser_register_new_object()
{
	static GtkWidget* window = NULL;
	object_registration_info* reg_info;

	if (!window)
	{
		GtkWidget *vbox;
		GtkWidget *iface_frame, *serverid_frame, *creation_frame;
		GtkWidget *hbox, *label;
		int i;
		
		window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

		/* make a variable to hold info about the object we're making,
		 * and allow for its destruction when the window's destroyed. */
		reg_info = (object_registration_info*)malloc(
			sizeof(object_registration_info));
		memset(reg_info, 0, sizeof(object_registration_info));

		reg_info->main_window = &window;
		
		fill_interface_hashtables (reg_info);
		
		/* do the rest of the window creation */
		gtk_signal_connect (GTK_OBJECT (window), "destroy",
				    GTK_SIGNAL_FUNC(delete_reg_info),
				    reg_info);

		gtk_window_set_title (GTK_WINDOW (window), "New Object");

		iface_frame = gtk_frame_new("Interface(s)");
		gtk_container_add (GTK_CONTAINER (iface_frame),
				   interface_section_new(reg_info));
		serverid_frame = gtk_frame_new("Server ID");
		gtk_container_add (GTK_CONTAINER (serverid_frame),
				   server_id_section_new(reg_info));	
		creation_frame = gtk_frame_new("Creation method");
		gtk_container_add (GTK_CONTAINER (creation_frame),
				   creation_type_section_new(reg_info));	
	
		vbox = gtk_vbox_new(FALSE, 5);
		gtk_box_pack_start (GTK_BOX (vbox),
				    iface_frame, FALSE, TRUE, 5);
		gtk_box_pack_start (GTK_BOX (vbox),
				    serverid_frame, FALSE, TRUE, 5);
		gtk_box_pack_start (GTK_BOX (vbox),
				    creation_frame, FALSE, TRUE, 5);
		hbox = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox),
				    hbox,
				    FALSE, TRUE, 5);		
		label = gtk_label_new ("Description:");
		gtk_box_pack_start (GTK_BOX (hbox),
				    label,
				    FALSE, TRUE, 5);		
		reg_info->description_entry = GTK_ENTRY (gtk_entry_new());
		gtk_box_pack_start (GTK_BOX (hbox),
				    GTK_WIDGET (reg_info->description_entry),
				    FALSE, TRUE, 5);
		
		/* add in `register object' btn */
		{
			GtkWidget *button, *hbox;
			button = gtk_button_new_with_label ("Register this object");

			hbox = gtk_hbox_new (FALSE, 0);
			gtk_box_pack_start (GTK_BOX (vbox),
					    hbox, FALSE, FALSE, 5);
			gtk_box_pack_start (GTK_BOX (hbox),
					    button, TRUE, FALSE, 5);

			gtk_signal_connect (GTK_OBJECT (button), "clicked",
					    GTK_SIGNAL_FUNC(register_object_clicked),
					    reg_info);
		}
		
		gtk_container_set_border_width (GTK_CONTAINER (window), 5);
		gtk_container_add (GTK_CONTAINER (window), vbox);
	}
	
	if (!GTK_WIDGET_VISIBLE (window)) {
		gtk_widget_show_all (window);
		gtk_widget_hide (GTK_WIDGET (reg_info->iface_add_uuid_button));
		gtk_widget_hide (GTK_WIDGET (reg_info->creation_browse_button));
	}
	else
	{	
		gtk_widget_destroy (window);
		window = NULL;
	}

	return window;	
} /* object_browser_register_new_object */

