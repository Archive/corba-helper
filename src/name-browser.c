/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/


#include <gnome.h>
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>

#include "corba-helper.h"
#include "name-bitmaps.h"

#define MAX_OBJECTS 100

extern GtkWidget* main_window;

struct pix_mask context_picture = {NULL,NULL};
struct pix_mask object_picture = {NULL,NULL};

/* debugging function to print a CosNaming_Name */
static void
CosNaming_Name_print(const CosNaming_Name *name)
{
	int i;

	for (i = 0; i < name->_length; i++)
		g_print("CosNamingName entry %d: id = %s, kind = %s\n",
			i, name->_buffer[i].id, name->_buffer[i].kind);
} /* CosNaming_Name_print */

/* copies one CosNaming_NameComponent to another */
static void
NameComponent__copy (CosNaming_NameComponent * dest,
		     const CosNaming_NameComponent * src)
{
  dest->id = CORBA_string_alloc (strlen (src->id));
  dest->kind = CORBA_string_alloc (strlen (src->kind));
  strcpy (dest->id, src->id);
  strcpy (dest->kind, src->kind);
} /* NameComponent__copy */

/* appends a new element to a CosNaming_Name, with id_in and kind_in
 * as defining the new element, and returns it; note that this
 * function DOESN'T free name_in */
static CosNaming_Name*
CosNaming_Name_append(const CosNaming_Name *name_in, const CORBA_char *id_in, const CORBA_char *kind_in)
{
	CosNaming_Name* name_out;
	int last_index;

	int i;

	name_out = (CosNaming_Name*)malloc(sizeof(CosNaming_Name));
	
	if (name_in)
		name_out->_length = name_in->_length + 1;
	
	else
		name_out->_length = 1;
	
	name_out->_buffer =
		CORBA_sequence_CosNaming_NameComponent_allocbuf (
			name_out->_length);
	
	for (i = 0; i < name_out->_length - 1; i++)
	{
		NameComponent__copy (name_out->_buffer + i,
				     name_in->_buffer + i);
	}

	last_index = name_out->_length - 1;

	name_out->_buffer[last_index].id =
		CORBA_string_alloc (strlen (id_in));

	name_out->_buffer[last_index].kind =
		CORBA_string_alloc (strlen (kind_in));
		
	strcpy (name_out->_buffer[last_index].id, id_in);
	strcpy (name_out->_buffer[last_index].kind, kind_in);	
	return name_out;
} /* CosNaming_Name_append */

static void
free_CosNaming_Name_cb(gpointer ptr)
{
	if (ptr)
	{
		CosNaming_Name *name = (CosNaming_Name*)ptr;
		
		name->_release = TRUE;
		CosNaming_Name__free(name, NULL, TRUE);
	}
} /* free_CosNaming_Name_cb */

/* helper function to make a ctree branch */
static GtkCTreeNode*
make_name_subtree(GtkCTree *root_treectrl, // required
	     GtkCTreeNode *mama_treenode, // can be NULL
	     gchar *label,
	     gchar *label2,
	     CosNaming_Name* resolved_name,
	     gboolean starts_open, 
	     GdkPixmap *pix_open, GdkBitmap *msk_open,
	     GdkPixmap *pix_closed, GdkBitmap *msk_closed) 
{
        GtkCTreeNode *new_subtree;
        gchar *text [2] = { label, label2 };

        new_subtree = gtk_ctree_insert_node (
                root_treectrl, mama_treenode,
                NULL, text, 5,
                pix_closed, msk_closed,
                pix_open, msk_open,
                FALSE,
                starts_open);

        gtk_ctree_node_set_row_data_full(
                root_treectrl, new_subtree,
                resolved_name, /* user data (in this case a filename) */
                free_CosNaming_Name_cb); /* destructor function for data */	
        return new_subtree;
} /* make_name_subtree */

/* this turns a binding (an element in the naming service tree)
 * into a ctree branch; if that binding contains other bindings,
 * it recurses to create more ctree branches */
static
void ProcessBinding(CORBA_Object name_service,
		    CosNaming_Binding *our_binding,
		    CosNaming_Name *parent_name,
		    GtkCTreeNode *parent_tree_node,
		    GtkCTree *treectrl_window)
{
	int i;
	CosNaming_NameComponent* name_component;
	GtkCTreeNode* our_ctree_node;
        GdkPixmap *icon_for_branch = NULL;
        GdkBitmap *icon_for_branch_mask = NULL;
	CosNaming_Name *our_resolved_name;
	
	name_component = our_binding->binding_name._buffer;

#if 0 // use icons	
	icon_for_branch = our_binding->binding_type == CosNaming_ncontext ?
		context_picture.pix : object_picture.pix;

	icon_for_branch_mask = our_binding->binding_type ==CosNaming_ncontext ?
		context_picture.mask : object_picture.mask;
#endif // use icons
	
	our_resolved_name = 
		CosNaming_Name_append(
			parent_name,
			name_component->id,
			name_component->kind);

	/* our_resolved_name now contains our name and that of parents;
	 * ex. {{"GNOME", "subcontext"},   <-- parent
	 *      {"Servers", "subcontext"}, <-- parent
	 *      {"my_server", "server"}};  <-- ours */

	/* create a ctree node from `our_binding' */
	our_ctree_node = make_name_subtree(
		treectrl_window, parent_tree_node,
		name_component->id, name_component->kind,
		our_resolved_name,
		TRUE, /* starts open  */
		icon_for_branch, icon_for_branch_mask,
		icon_for_branch, icon_for_branch_mask);
	
	/* if it's not a context, it's an object reference,
	   which can't contain more bindings */
	if (our_binding->binding_type != CosNaming_ncontext)
		return;
	else /* this binding may contain other bindings */
	{    
		CORBA_Object our_context;
		CORBA_Environment ev;
		CORBA_exception_init(&ev);		

		our_context = CosNaming_NamingContext_resolve(
			name_service, our_resolved_name, &ev);

		if(ev._major == CORBA_NO_EXCEPTION) {
			CosNaming_BindingList *binding_list;
			CosNaming_BindingIterator binding_iterator;
			
			CosNaming_NamingContext_list(
				our_context, MAX_OBJECTS,
				&binding_list,
				&binding_iterator/* = NULL */, &ev);

			/* iterate recursively through bindings */
			for (i = 0; i < binding_list->_length; i++)
			{
				ProcessBinding(name_service,
					       &(binding_list->_buffer[i]),
					       our_resolved_name,
					       our_ctree_node,
					       treectrl_window);
			}

			CORBA_Object_release(our_context, &ev);
			CORBA_exception_free(&ev);

		}
		else
	 		g_print("NamingContext_Resolve failed!\n");
	}

} /* ProcessBinding */


/* refill our tree control with a hierarchy from the items
 * in the nameserver */
static void
RefillTree(GtkWidget *refresh_button,
	   GtkCTree *treectrl_window)
{
	CosNaming_BindingIterator binding_iterator;
	CosNaming_BindingList* binding_list;	
	CORBA_Object nameserver = CORBA_OBJECT_NIL;
	CORBA_Environment ev;
	GtkCTreeNode *root_tree_node;	
	int i;

	g_assert(treectrl_window);

	nameserver = gnome_name_service_get();
	g_assert(nameserver != CORBA_OBJECT_NIL);
	
	CORBA_exception_init(&ev);
	CosNaming_NamingContext_list (nameserver, MAX_OBJECTS, &binding_list,
				     &binding_iterator/* = NULL */, &ev);

	gtk_clist_freeze (GTK_CLIST (treectrl_window));
	gtk_clist_clear (GTK_CLIST(treectrl_window));

	root_tree_node = make_name_subtree(
		treectrl_window, NULL,
		"[root context]", "",
		NULL,
		TRUE, /* starts open  */
		NULL, NULL, NULL, NULL);
	
	for (i = 0; i < binding_list->_length; i++) {
		/* for each item ( = binding) in our root context,
		 * add a ctree branch; ProcessBinding then recurses */
		ProcessBinding(nameserver,
			       &(binding_list->_buffer[i]),
			       NULL, root_tree_node,
			       treectrl_window);
	}

	/* cleanup */
	CORBA_Object_release(nameserver, &ev);
	CORBA_exception_free(&ev);

	gtk_clist_thaw (GTK_CLIST (treectrl_window));
} /* RefillTree */

GtkWidget*
nameservice_browser_new()
{
	GtkWidget *vbox, *hbox;
	GtkWidget *refresh_button = NULL;
	GtkWidget *scrolled_treewin;
	GtkCTree *treectrl_window;
	GdkColor transparent;
	
	
	char *ctree_titles[] = { "identifier" , "kind" };

        vbox = gtk_vbox_new (FALSE, 0);	
        	
        /* make a scrolled window for the treectrl, and put it in the vbox */
        scrolled_treewin = gtk_scrolled_window_new (NULL, NULL);
        gtk_container_set_border_width (GTK_CONTAINER (scrolled_treewin), 5);
        gtk_scrolled_window_set_policy (
		GTK_SCROLLED_WINDOW (scrolled_treewin),
		GTK_POLICY_AUTOMATIC,
		GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vbox), scrolled_treewin, TRUE, TRUE, 0);

        /* make a ctree control & put it in the scrolled window */
        treectrl_window = GTK_CTREE (
		gtk_ctree_new_with_titles(2, 0, ctree_titles));
        gtk_clist_set_column_auto_resize (GTK_CLIST (treectrl_window),
					  0, TRUE);	
        gtk_clist_set_column_width (GTK_CLIST (treectrl_window),
				    1, 200);
        gtk_clist_set_selection_mode (
		GTK_CLIST (treectrl_window),
		GTK_SELECTION_SINGLE);
        gtk_ctree_set_line_style (treectrl_window,
				  GTK_CTREE_LINES_DOTTED);	
        gtk_widget_set_usize (GTK_WIDGET (treectrl_window),
			      TREECTRL_WIDTH, TREECTRL_HEIGHT);
        gtk_container_add (GTK_CONTAINER (scrolled_treewin),
                           GTK_WIDGET (treectrl_window));


        /* create the pixmaps that will go to the left of treectrl items */
	if (!context_picture.pix)
		context_picture.pix = gdk_pixmap_create_from_xpm_d (
			main_window->window, &context_picture.mask,
			&transparent, context_xpm);

	if (!object_picture.pix)		
		object_picture.pix = gdk_pixmap_create_from_xpm_d (
			main_window->window, &object_picture.mask,
			&transparent, object_xpm);
	
        /* put refresh_button in the vbox */
	hbox = gtk_hbox_new (FALSE, 0);
	
	refresh_button = gtk_button_new_with_label ("refresh");
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), refresh_button, TRUE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT (refresh_button), "clicked",
			    GTK_SIGNAL_FUNC(RefillTree),
			    treectrl_window);


	RefillTree(NULL, treectrl_window);
	
        return vbox;

} /* nameservice_browser_new */




