/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

/* much of this code in this file is stolen
 * from the goad-browser */

#include <gnome.h>
#include <orb/orbit.h>
#include <libgnorba/gnorba.h>

#include "corba-helper.h"

typedef enum {
	SORTBY_DESCRIPTION, SORTBY_ACTIVATION_TYPE, SORTBY_INTERFACE
} sorting_key;

/* makes a subtree in a tree control */
static GtkCTreeNode*
make_obj_subtree(GtkCTree *root_treectrl,
		 GtkCTreeNode *mama_treenode,
		 gchar *label1, gchar *label2,
		 gboolean starts_open)
{
	GtkCTreeNode *new_subtree;
	gchar *text[2] = { label1, label2};

	new_subtree = gtk_ctree_insert_node (
		root_treectrl, mama_treenode,
		NULL, text, 5,
		NULL, NULL, NULL, NULL,
		FALSE, starts_open);

	return new_subtree;
} /* make_obj_subtree */

static void
RegisterNewObject_clicked(GtkWidget *new_object_button,
			  gpointer pOur_list)
{
	object_browser_register_new_object();
} /* RegisterNewObject_clicked */


/* for each server found, insert it into the tree ctrl
 * with the `description' field */
static void
RefillByDescription(GoadServer* servers, GtkCTree *ctree)
{
	int i;
	
	for(i = 0; servers[i].repo_id; i++) {

		GtkCTreeNode *this_branch;
		gchar *repo_ids;
		gchar *type;		

		this_branch = make_obj_subtree (
			ctree, NULL,
			servers[i].description, "",
			FALSE);

		make_obj_subtree(
			ctree, this_branch,
			"server id",
			servers[i].server_id,
			FALSE);

		make_obj_subtree(
			ctree, this_branch,
			"location info",
			servers[i].location_info,
			FALSE);
		
		repo_ids = g_strjoinv(",",servers[i].repo_id);
		
		make_obj_subtree(
			ctree, this_branch,
			"repo ids",
			repo_ids,
			FALSE);

		g_free(repo_ids);
		
		switch(servers[i].type) {
		case GOAD_SERVER_SHLIB: type = "loaded from shared library"; break;
		case GOAD_SERVER_EXE: type = "executable"; break;
		case GOAD_SERVER_RELAY: type = "relay"; break;
		case GOAD_SERVER_FACTORY: type = "creatable via factory"; break;		
		default:
			type = "[none]";
		}
      
		make_obj_subtree(
			ctree, this_branch,
			"type",
			type,
			FALSE);
	}
} /* RefillByDescription */

/* for each server found, insert it one of four branches:
 * exe, shlib, factory, or unknown. Each one corresponds
 * to an activation type */
static void
RefillByActivationType(GoadServer* servers, GtkCTree *ctree)
{
	int i;
	GtkCTreeNode *exe_branch, *relay_branch, *shlib_branch, *factory_branch;
	GtkCTreeNode *unknown_branch;
	
	exe_branch = relay_branch = shlib_branch = factory_branch = NULL;
	unknown_branch = NULL;

	for(i = 0; servers[i].repo_id; i++) {

		GtkCTreeNode *this_branch, *parent;
		gchar *repo_ids;

		switch (servers[i].type) {
		case GOAD_SERVER_EXE:
			if (!exe_branch)
				exe_branch = make_obj_subtree(
					ctree, NULL,
					"executable", "", FALSE);
			parent = exe_branch;
			break;
		case GOAD_SERVER_SHLIB:
			if (!shlib_branch)
				shlib_branch = make_obj_subtree(
					ctree, NULL,
					"loaded from shared library", "", FALSE);
			parent = shlib_branch;
			break;
		case GOAD_SERVER_FACTORY:
			if (!factory_branch)
				factory_branch = make_obj_subtree(
					ctree, NULL,
					"created via factory", "", FALSE);
			parent = factory_branch;
			break;			
		case GOAD_SERVER_RELAY:					
			if (!relay_branch)
				relay_branch = make_obj_subtree(
					ctree, NULL,
					"relay", "", FALSE);
			parent = relay_branch;
			break;
			
		default:
			if (!unknown_branch)
				unknown_branch = make_obj_subtree(
					ctree, NULL,
					"unknown", "", FALSE);
			parent = unknown_branch;
			break;						
		}
		
		this_branch = make_obj_subtree (
			ctree, parent,
			servers[i].description, "",
			FALSE);

		make_obj_subtree(
			ctree, this_branch,
			"server id",
			servers[i].server_id,
			FALSE);

		make_obj_subtree(
			ctree, this_branch,
			"location info",
			servers[i].location_info,
			FALSE);
		
		repo_ids = g_strjoinv(",",servers[i].repo_id);
		
		make_obj_subtree(
			ctree, this_branch,
			"repo ids",
			repo_ids,
			FALSE);

		g_free(repo_ids);
	}
} /* RefillByActivationType */


/* for each interface found, make a branch; then,
 * for each server found, insert it into the branch that
 * corresponds to its interface */
static void
RefillByInterface(GoadServer* servers, GtkCTree *ctree)
{
	int i;

	GHashTable *interface_hash;
	interface_hash = g_hash_table_new (g_str_hash, g_str_equal);

	/* for each server... */
	for (i = 0; servers[i].repo_id; i++) {
		int j;

		/* for each repo id that a server has... */
		for (j = 0; servers[i].repo_id[j]; j++)
		{

			/* insert it into the interface_hash,
			 * and add a tree branch to represent it */
			if (!g_hash_table_lookup (interface_hash,
						  servers[i].repo_id[j]))
			{
				
				GtkCTreeNode *branch = make_obj_subtree(
					ctree, NULL,
					servers[i].repo_id[j], "",
					FALSE);
			
				g_hash_table_insert (interface_hash,
						     servers[i].repo_id[j],
						     branch);
			}
		}			
	}

	/* for each server... */
	for (i = 0; servers[i].repo_id; i++) {
		int j;

		/* for each repo id that a server has... */
		for (j = 0; servers[i].repo_id[j]; j++)
		{
			gchar *type;
			GtkCTreeNode *interface_node;
			GtkCTreeNode *our_desc_branch;

			/* insert the server's info into the correct
			 * interface branch */
			   
			interface_node = g_hash_table_lookup (
				interface_hash,
				servers[i].repo_id[j]);

			g_assert(interface_node);

			our_desc_branch = make_obj_subtree (
				ctree, interface_node,
				servers[i].description, "",
				FALSE);

			make_obj_subtree(
				ctree, our_desc_branch,
				"server id",
				servers[i].server_id,
				FALSE);

			make_obj_subtree(
				ctree, our_desc_branch,
				"location info",
				servers[i].location_info,
				FALSE);
		
			switch(servers[i].type) {
			case GOAD_SERVER_SHLIB:
				type = "loaded from shared library"; break;
			case GOAD_SERVER_EXE:
				type = "executable"; break;
			case GOAD_SERVER_RELAY:
				type = "relay"; break;
			case GOAD_SERVER_FACTORY:
				type = "creatable via factory"; break;
			default:
				type = "[none]";
			}
      
			make_obj_subtree(
				ctree, our_desc_branch,
				"type",
				type,
				FALSE);			
			
			if (!g_hash_table_lookup (interface_hash,
						  servers[i].repo_id[j]))
			{
				GtkCTreeNode *branch = make_obj_subtree(
					ctree, NULL,
					servers[i].repo_id[j], "",
					FALSE);
			
				g_hash_table_insert (interface_hash,
						     servers[i].server_id,
						     branch);
			}
		}			
	}	

	g_hash_table_destroy (interface_hash);
	
	return;
} /* RefillByInterface */

/* refill the list of servers in our tree ctrl */
static void
RefillServerList(GtkCTree *treectrl_window, sorting_key sort_style)
{
	GoadServerList *servlist;
	GoadServer* servers;	
	CORBA_Environment ev;
	int maxw[5];
	
	CORBA_exception_init(&ev);

	gtk_clist_freeze(GTK_CLIST(treectrl_window));
	gtk_clist_clear(GTK_CLIST(treectrl_window));	

	servlist = goad_server_list_get();
	servers = servlist->list;

	memset(maxw, 0, sizeof(maxw));

	switch (sort_style)
	{
	case SORTBY_ACTIVATION_TYPE:
		RefillByActivationType (servers, treectrl_window);
		break;
	case SORTBY_INTERFACE:
		RefillByInterface (servers, treectrl_window);
		break;
	case SORTBY_DESCRIPTION:
		RefillByDescription (servers, treectrl_window);
		break;
	default:
		g_assert_not_reached();
	}
	
	
	gtk_clist_thaw(GTK_CLIST(treectrl_window));
	CORBA_exception_free(&ev);
		
	goad_server_list_free(servlist);

} /* RefillServerList */

/* set our browser->key to represent that we want to sort
 * by the `description' field, and then refill the tree */
static void
description_radio_clicked(GtkWidget *button,
			  GtkCTree* treectrl_window)
{
	g_assert (treectrl_window);
	
	if (!(GTK_TOGGLE_BUTTON(button))->active) return;

	RefillServerList (treectrl_window, SORTBY_DESCRIPTION);	
} /* description_radio_clicked */

/* set our browser->key to represent that we want to sort
 * by the `activation_type' field, and then refill the tree */
static void
activation_type_radio_clicked(GtkWidget *button,
			      GtkCTree* treectrl_window)
{
	g_assert (treectrl_window);	
	
	if (!(GTK_TOGGLE_BUTTON(button))->active) return;

	RefillServerList (treectrl_window, SORTBY_ACTIVATION_TYPE);	
} /* activation_type_radio_clicked */

/* set our browser->key to represent that we want to sort
 * by the `interface' field, and then refill the tree */
static void
interface_radio_clicked(GtkWidget* button,
			GtkCTree* treectrl_window)
{
	g_assert (treectrl_window);
	
	if (!(GTK_TOGGLE_BUTTON(button))->active) return;

	RefillServerList (treectrl_window, SORTBY_INTERFACE);
} /* interface_radio_clicked */



GtkWidget*
object_browser_new()
{
	GtkWidget *vbox_rtn, *hbox;
	GtkWidget *new_object_btn;
	GtkWidget *radio_vbox; /* holds the radio btns */
	GtkWidget *radio_button;
	GtkWidget *label;
	GtkWidget *scrolly; /* holds the treectrl_window */
	GtkCTree *treectrl_window;

	char *ctree_titles[] = { "variable" , "value" };

	/* the object-browser will be contained in this vbox */
	vbox_rtn = gtk_vbox_new(FALSE, 0);
	
	/* create a scrolled window for the tree ctrl */
	scrolly = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolly),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	
	gtk_box_pack_start (GTK_BOX (vbox_rtn), scrolly, TRUE, TRUE, 5);

	/* create a tree control, and put it in the scrolled window */
	treectrl_window = GTK_CTREE (gtk_ctree_new_with_titles(2, 0, ctree_titles));

	gtk_widget_set_usize (GTK_WIDGET (treectrl_window),
			      TREECTRL_WIDTH, TREECTRL_HEIGHT);
        gtk_clist_set_column_auto_resize (GTK_CLIST (treectrl_window),
					  0, TRUE);
	gtk_container_add (GTK_CONTAINER (scrolly), GTK_WIDGET(treectrl_window));
	

	radio_vbox = gtk_hbox_new (TRUE, 0);

	/* add a button that allows us to register new objects */
	new_object_btn = gtk_button_new_with_label("Register new object");
	gtk_signal_connect (GTK_OBJECT (new_object_btn), "clicked",
			    GTK_SIGNAL_FUNC(RegisterNewObject_clicked),
			    radio_vbox);	
	hbox = gtk_hbox_new(FALSE, 0);	
	gtk_box_pack_start (GTK_BOX (hbox), new_object_btn, TRUE, FALSE, 0);

	/* create the set of radio buttons that will allow us to sort
	 * the tree control items by description, interface, or activation */
	label = gtk_label_new ("Sort by:");
	gtk_box_pack_start (GTK_BOX (radio_vbox), label, FALSE, FALSE, 0);
	
	radio_button = gtk_radio_button_new_with_label (
		NULL, "Description");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC (description_radio_clicked),
			    treectrl_window);
	gtk_box_pack_start (GTK_BOX (radio_vbox), radio_button, FALSE, TRUE, 0);

	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"Interface");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC (interface_radio_clicked),
			    treectrl_window);
	gtk_box_pack_start (GTK_BOX (radio_vbox), radio_button, FALSE, FALSE, 0);
	
	radio_button = gtk_radio_button_new_with_label (
		gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button)),
		"Activation type");
	gtk_signal_connect (GTK_OBJECT (radio_button), "clicked",
			    GTK_SIGNAL_FUNC (activation_type_radio_clicked),
			    treectrl_window);
	gtk_box_pack_start (GTK_BOX (radio_vbox), radio_button, FALSE, TRUE, 0);

	/* put things the radio buttons in their place */
	gtk_box_pack_start (GTK_BOX (vbox_rtn), radio_vbox, FALSE, TRUE, 5);
	gtk_box_pack_start (GTK_BOX (vbox_rtn), hbox, FALSE, TRUE, 5);
	
	RefillServerList(treectrl_window, SORTBY_DESCRIPTION);
	
        return vbox_rtn;
} /* object_browser_new */



