/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

#include <gnome.h>

/**
 * message_box:
 * @format: printf-style formatting string
 *
 * Brings up a simple, modal dialog box with an `OK' button.
 */
void
message_box (gchar *format, ...)
{
	GtkWidget *dlg;
	GtkBox *hbox;
	GtkBox *vbox;
	GtkWidget *label, *btn;
	va_list args;
	char *str;

	g_assert (format);
	
	va_start (args, format);
	str = g_strdup_vprintf (format, args);
	va_end (args);
  	
	dlg = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	vbox = GTK_BOX (gtk_vbox_new (FALSE, 5));
	gtk_container_add (GTK_CONTAINER (dlg), GTK_WIDGET(vbox));

 	hbox = GTK_BOX (gtk_hbox_new (FALSE, 5));
	label = gtk_label_new (str);
	gtk_box_pack_start (hbox, label, TRUE, TRUE, 5);
	gtk_box_pack_start (vbox, GTK_WIDGET (hbox), TRUE, TRUE, 5);
	
 	hbox = GTK_BOX (gtk_hbox_new (FALSE, 5));
	btn = gtk_button_new_with_label (_("OK"));
	gtk_box_pack_start (hbox, btn, TRUE, TRUE, 5);
	gtk_box_pack_start (vbox, GTK_WIDGET (hbox), FALSE, TRUE, 5);

	gtk_widget_show_all (dlg);
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &dlg);
	gtk_signal_connect_object (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC(gtk_widget_destroy),
			    GTK_OBJECT (dlg));
	g_free (str);
} /* message_box */

/**
 * contains_non_whitespace
 * @str: NULL-terminated string to test
 *
 * Returns TRUE if @str contains a non-whitespace character 
 */
gboolean
contains_non_whitespace (gchar *str)
{
	int i;

	g_assert(str);
	
	for (i = 0; i < strlen (str); i++)
		if (!isspace (str[i])) return TRUE;
	
	return FALSE;
} /* contains_non_whitespace */


/**
 * remove_leading_and_trailing_whitespace:
 * @str: NULL-terminated input string 
 *
 * Constructs a new string (based on @str) which has no
 * leading or trailing whitespace, and returns that string.
 * It must be freed by the caller.
 */
gchar*
remove_leading_and_trailing_whitespace (gchar *str)
{
	gchar *rtn = NULL;
	int i, len = 0;

	g_assert (str);
	
	for (i = 0; i < strlen (str) && !rtn; i++) {
		if (!isspace (str[i]))
			rtn = &str[i];
	}

	for (len = strlen (str) - 1; len >= 0; len--) {

		if (!isspace (rtn[len])) {
			len++;
			break;
		}
	}

	return (len==0)?(gchar*)NULL:g_strndup (rtn, len);
} /* remove_leading_and_trailing_whitespace */

/**
 * contains_whitespace:
 * @str: NULL-terminated string to test
 *
 * Returns true if any character in a string is a whitespace.
 */
gboolean contains_whitespace (gchar *str)
{
	int i;
	g_assert (str);
	for (i = 0; i < strlen (str); i++) {
		if (isspace (str[i]))
			return TRUE;
	}

	return FALSE;
} /* contains_whitespace */


/**
 * convert_to_alphanumeric
 * @str: NULL-terminated string to convert
 *
 * Turns any non-alphanumeric character in a string
 * into an underscore.
 */
void
convert_to_alphanumeric (gchar *str)
{
	int i;
	g_assert(str);

	for (i = 0; i < strlen (str); i++) {
		if (!isalpha (str[i]))
			str[i] = '_';
	}
} /* convert_to_alphanumeric */
